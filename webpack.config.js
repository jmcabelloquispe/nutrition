const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const javascriptRules = {
  test: /\.m?js$/,
  exclude: /(node_modules|bower_components)/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env']
    }
  }
}

const htmlRules = {
  test: /\.pug$/,
  use: ['pug-loader'],
}

const cssRules = {
  test: /\.s[ac]ss$/i,
  use: [
    'style-loader',
    'css-loader',
    'sass-loader',
  ],
}
const imageRules = {
  test: /\.(png|svg|jpg|gif)$/,
  loader: 'file-loader',
  options: {
    name: '[name].[ext]',
    outputPath: 'images'
  }
}

module.exports = {
  entry: {
    home: './src/javascript/home/app.js',
    login: './src/javascript/login/app.js',
    'sign-up': './src/javascript/sign-up/app.js'
  },
  output: {
    filename: 'js/[name].[contenthash].js'
  },
  module: {
    rules: [
      javascriptRules,
      htmlRules,
      cssRules,
      imageRules
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/layouts/home/index.pug',
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      filename: 'login.html',
      template: './src/layouts/login/index.pug',
      chunks: ['login']
    }),
    new HtmlWebpackPlugin({
      filename: 'sign-up.html',
      template: './src/layouts/sign-up/index.pug',
      chunks: ['sign-up']
    })
  ]
}
